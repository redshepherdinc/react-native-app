## Getting started & Setting up your dev env

### For iOS Install cocopods
```
sudo gem install cocoapods
```

### Setting up your ANDROID_HOME enviroment
```
vi ~/.zprofile
```
Add these two lines
export ANDROID_HOME=/Users/kumar/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools


### Failed to install the following Android SDK packages as some licences have not been accepted.
Run this command
```
export JAVA_HOME=/Applications/Android\ Studio.app/Contents/jre/jdk/Contents/Home
yes | ~/Library/Android/sdk/tools/bin/sdkmanager --licenses
```
---
---

## Clone from repo
```
git clone git clone https://redshep@bitbucket.org/redshepherdinc/react-native-app.git
```
---
---


## Common Errors & Settings that need to be done

### Adding Custom fonts
Add fonts to assets/fonts folder in the project
Follow these articles
https://medium.com/@kswanie21/custom-fonts-in-react-native-tutorial-for-ios-android-76ceeaa0eb78
https://medium.com/@mehran.khan/ultimate-guide-to-use-custom-fonts-in-react-native-77fcdf859cf4

Create File “react-native.config.js” and add following code

```
module.exports = {
  project: {
    ios: {},
    android: {}, // grouped into "project"
  },
  assets: ["./assets/fonts/"], // stays the same
};
```

Then run
```
npx react-native link
```

## Android Build

Could not initialize class org.codehaus.groovy.runtime.InvokerHelper
https://stackoverflow.com/questions/35000729/android-studio-could-not-initialize-class-org-codehaus-groovy-runtime-invokerhel

In gradle-wrapper.properties please use grade version 6.3 or above.
For e.g:distributionUrl=https\://services.gradle.org/distributions/gradle-6.3-all.zip
---
Read this article about Crypto js version set to 3.3.0 
... any further versions are not supported in react native
https://github.com/brix/crypto-js/issues/256
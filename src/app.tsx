import React from 'react';
import { StatusBar } from 'react-native';

// screens
import Screen_NewPayment from './screens/s_new_payment';

const App = () => {
  return (
    <>
      <StatusBar hidden />
      <Screen_NewPayment />
    </>
  );
};

export default App;

import React, { useEffect, useState } from 'react';
import { View, TextInput, Image, Text } from 'react-native';

// elements
import Button from '../elements/button';

import styles from '../styles/common';
import { deviceWidth, colors } from '../styles/variables';

// redpay
import { RedPayProcessor, Config } from '../modules/redpay';
import {
  getCardType,
  addSpacers,
  getCardNumber,
  getAmountInCents,
  getExpMMYYYY,
  isCardValid,
  isExpirationValid,
  isValidEmail
} from '../modules/card_helper';

export interface Props {
  screen?: string;
}

const card_icons = {
  emptycard: require('../../assets/images/icon_emptycard.png'),
  Visa: require('../../assets/images/icon_visa.png'),
  MasterCard: require('../../assets/images/icon_mastercard.png'),
  DinersClub: require('../../assets/images/icon_dinersclub.png'),
  CarteBlanche: require('../../assets/images/icon_carteblanche.png'),
  AmEx: require('../../assets/images/icon_amex.png'),
  Discover: require('../../assets/images/icon_discover.png'),
  JCB: require('../../assets/images/icon_jcb.png'),
  enRoute: require('../../assets/images/icon_enroute.png'),
  Solo: require('../../assets/images/icon_solo.png'),
  Switch: require('../../assets/images/icon_switch.png'),
  Maestro: require('../../assets/images/icon_maestro.png'),
  VisaElectron: require('../../assets/images/icon_visaelectron.png'),
  LaserCard: require('../../assets/images/icon_laser.png')
};

const CardInput = (props: Props) => {
  const [CC, setCC] = useState('');
  const [err_CC, setErrCC] = useState('');

  const [MMYY, setMMYY] = useState('');
  const [err_MMYY, setErrMMYY] = useState('');

  const [CVV, setCVV] = useState('');
  const [err_CVV, setErrCVV] = useState('');

  const [ZIP, setZIP] = useState('');
  const [err_ZIP, setErrZIP] = useState('');

  const [NAME, setNAME] = useState('');
  const [err_NAME, setErrNAME] = useState('');

  const [AMT, setAMT] = useState('');
  const [centAMT, setcentAMT] = useState('');
  const [err_AMT, setErrAMT] = useState('');

  const [EMAIL, setEMAIL] = useState('');
  const [err_EMAIL, setErrEMAIL] = useState('');

  const [PHONE, setPHONE] = useState('');
  const [err_PHONE, setErrPHONE] = useState('');

  const [cardIcon, setCardIcon] = useState(card_icons.emptycard);

  const [busy, setBusy] = useState(false);
  const [result, setResult] = useState('');

  function validate() {
    //Reset errors
    setErrCC('');
    setErrMMYY('');
    setErrCVV('');
    setErrZIP('');
    setErrNAME('');
    setErrPHONE('');
    setErrEMAIL('');
    setErrAMT('');
    setResult('');

    var valid = true;

    // Validate CC
    if (isCardValid(CC) !== '') {
      setErrCC('invalid card number');
      valid = false;
    }

    // Validate ExpMMYY
    var mmyy = MMYY.split('/');
    if (mmyy.length !== 2) {
      setErrMMYY('invalid mmyy');
      valid = false;
    } else {
      var mm = mmyy[0].trim();
      var yyyy = '20' + mmyy[1].trim();
      if (!isExpirationValid(Number(mm), Number(yyyy))) {
        setErrMMYY('invalid mmyy');
        valid = false;
      }
    }

    // Validate CVV
    if (CVV.length < 3) {
      setErrCVV('invalid cvv');
      valid = false;
    }

    // Validate Zip
    if (ZIP.length < 5) {
      setErrZIP('invalid zip');
      valid = false;
    }

    // Validate Name on Card
    if (NAME.length === 0) {
      setErrNAME('name required');
      valid = false;
    }

    // Validate Email
    if (EMAIL.length > 0) {
      if (!isValidEmail(EMAIL)) {
        setErrEMAIL('invalid email');
        valid = false;
      }
    }

    // Validate Amount
    if (AMT.length === 0) {
      setErrAMT('amount required');
      valid = false;
    } else {
      let amt = Number(AMT.replace('$', ''));
      if (amt <= 0) {
        setErrAMT('amount required');
        valid = false;
      }
    }

    return valid;
  }

  function submit() {
    if (!validate()) return;

    setBusy(true);

    let config: Config = {
      app: 'DEMO',
      url: 'https://redpaystable.azurewebsites.net/',
      key:
        'MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAtsQxNp3vmKVNYIxfWSi0LIRgCnPaMn0MUNouxgrs4zmg4cnvSeQ3I8YP03YbpXuWA80RvOw/nWErYAKomniJw8Y+xexMfBQ5sgJgewn3ZnRPNM9Y4Z62gwfIlsrs7Bwvpz9uUtLgeQLl1ffNaumnu1IBrqRps0EZ1QyDuu41UckTyo31C40Wez6IbeMfZeusrmPlIWqyBacdviJ5zHCA3zHNq86QMnB8HOP1U81HOSs6GTTelhD7lCoJ+fHKHxcz0MDr37fNpKpC57B0/20wBXFp9tlVtSkHcIty1lyNk2/HDH8knCdqkZk+fCvWgGwdex41x8/rM+LKC13c5J/yG6Gb2PnKhwNk4lvvnz73YAdqTUJ7qNrdtWVnOTWfbMBiNlpBCVqt8xY8UK6u83AVWrWXse0xe2Pn/kRqlXmxWT0mGEoCavjvZ9lQUL7LXAXZ1dff9r+oFUZo6xDQ3ER/OTIKa4jpvaI9S/J1drsrI1f9kkMWFwEh48dCPYplGSxzAgMBAAE='
    };

    let req = {
      account: getCardNumber(CC),
      amount: getAmountInCents(AMT),
      expmmyyyy: getExpMMYYYY(MMYY),
      cvv: CVV,
      cardHolderName: NAME,
      avsZip: ZIP
    };

    console.info('PAYMENT REQUEST >>>');
    console.info(JSON.stringify(req, null, 2));

    RedPayProcessor.Charge(config, req)
      .then((res) => {
        console.info('PAYMENT RESPONSE >>>');
        console.info(JSON.stringify(res, null, 2));
        setBusy(false);
        setResult('SUCCESS');
        setAMT(''); // Reset Amount
      })
      .catch((err) => {
        console.error('PAYMENT ERROR >>>');
        console.error(err);
        setBusy(false);
        setResult('ERROR');
      });
  }

  function fontSizer(screenWidth: number) {
    if (screenWidth > 400) {
      return 22;
    } else if (screenWidth > 250) {
      return 18;
    } else {
      return 14;
    }
  }
  const fontSize = fontSizer(deviceWidth);

  function keyPressCardNumber(e: any) {
    let key = e.nativeEvent.key;

    let _cardType = getCardType(CC);
    let _cardName = _cardType && _cardType.name ? _cardType.name : null;
    let _cardNumber = getCardNumber(CC);

    if (key !== 'Backspace') {
      if (_cardName === 'AmEx') {
        if (_cardNumber.length >= 15) {
          e.stopPropagation();
          return;
        }
      } else {
        if (_cardNumber.length >= 16) {
          e.stopPropagation();
          return;
        }
      }
    }

    if (key === 'Backspace') {
      if (CC.endsWith(' ') && CC.length > 2) {
        setCardNumber(CC.substring(0, CC.length - 2));
      } else if (CC.length > 0) setCardNumber(CC.substring(0, CC.length - 1));
    } else if (isNaN(key)) {
      e.stopPropagation();
    } else {
      setCardNumber(CC + key);
    }
  }

  function setCardNumber(cc: string) {
    let _cardType = getCardType(cc);
    let _cardName = _cardType && _cardType.name ? _cardType.name : null;

    switch (_cardName) {
      case 'Visa':
        setCardIcon(card_icons.Visa);
        break;
      case 'MasterCard':
        setCardIcon(card_icons.MasterCard);
        break;
      case 'DinersClub':
        setCardIcon(card_icons.DinersClub);
        break;
      case 'CarteBlanche':
        setCardIcon(card_icons.CarteBlanche);
        break;
      case 'AmEx':
        setCardIcon(card_icons.AmEx);
        break;
      case 'Discover':
        setCardIcon(card_icons.Discover);
        break;
      case 'JCB':
        setCardIcon(card_icons.JCB);
        break;
      case 'enRoute':
        setCardIcon(card_icons.enRoute);
        break;
      case 'Solo':
        setCardIcon(card_icons.Solo);
        break;
      case 'Switch':
        setCardIcon(card_icons.Switch);
        break;
      case 'Maestro':
        setCardIcon(card_icons.Maestro);
        break;
      case 'VisaElectron':
        setCardIcon(card_icons.VisaElectron);
        break;
      case 'LaserCard':
        setCardIcon(card_icons.LaserCard);
        break;
      default:
        setCardIcon(card_icons.emptycard);
        break;
    }

    cc = addSpacers(cc, _cardType, ' ');
    setCC(cc);

    let _cardNumber = getCardNumber(cc);
    if (_cardName == 'AmEx') {
      if (_cardNumber.length >= 15) {
        setErrCC(isCardValid(_cardNumber));
      } else setErrCC('');
    } else {
      if (_cardNumber.length >= 16) {
        setErrCC(isCardValid(_cardNumber));
      } else setErrCC('');
    }
  }

  function keyPressExpMMYY(e: any) {
    setErrMMYY('');

    let key = e.nativeEvent.key;
    if (key === 'Backspace') {
      if (MMYY.length !== 0) {
        if (MMYY.endsWith(' / ')) setMMYY(MMYY.substring(0, 1));
        else setMMYY(MMYY.substring(0, MMYY.length - 1));
      }
    } else if (isNaN(key)) {
      e.stopPropagation();
      return;
    } else if (MMYY.length === 0) {
      if (key === '0' || key === '1') {
        // first digit can only be 1 or 0
        setMMYY(MMYY + key);
      } else {
        e.stopPropagation();
        return;
      }
    } else if (MMYY.length === 1) {
      if (MMYY === '1' && (key === '1' || key === '2')) {
        // second digit can only be 1 or 2 if first is 1
        setMMYY(MMYY + key + ' / ');
      } else if (key === '0' && MMYY === '0') {
        //00 is not valid month
        e.stopPropagation();
        return;
      } else {
        setMMYY(MMYY + key + ' / ');
      }
    } else if (MMYY.length < 7) {
      setMMYY(MMYY + key);

      if ((MMYY + key).length === 7) {
        var mmyy = MMYY + key;
        let MM = mmyy.split(' / ')[0];
        let YY = mmyy.split(' / ')[1];

        let err = '';
        try {
          let mm = Number(MM);
          let yy = Number(YY);
          let cy = Number(new Date().getFullYear().toString().substr(-2));
          let cm = Number(new Date().getMonth()) + 1;
          if (mm > 12) {
            err = 'Invalid MM';
          }
          if (yy < cy) {
            err = 'Invalid YY';
          }
          if (yy == cy && mm < cm) {
            err = 'Invalid MM';
          }
        } catch (e) {
          err = 'Invalid Exp Date';
        }

        setErrMMYY(err);
      }
    }
  }

  function keyPressCVV(e: any) {
    setErrCVV('');

    let key = e.nativeEvent.key;
    if (key === 'Backspace') {
      if (CVV.length !== 0) {
        setCVV(CVV.substring(0, CVV.length - 1));
      }
    } else if (isNaN(key)) {
      e.stopPropagation();
      return;
    } else if (CVV.length < 4) {
      setCVV(CVV + key);
    } else {
      e.stopPropagation();
      return;
    }
  }

  function keyPressZIP(e: any) {
    setErrZIP('');

    let key = e.nativeEvent.key;
    if (key === 'Backspace') {
      if (ZIP.length !== 0) {
        setZIP(ZIP.substring(0, ZIP.length - 1));
      }
    } else if (isNaN(key)) {
      e.stopPropagation();
      return;
    } else if (ZIP.length < 5) {
      setZIP(ZIP + key);
    } else {
      e.stopPropagation();
      return;
    }
  }

  function keyPressAMT(e: any) {
    setErrAMT('');

    let key = e.nativeEvent.key;
    if (key === 'Backspace') {
      if (centAMT.length !== 0) {
        let _cents = centAMT.substring(0, centAMT.length - 1);
        setcentAMT(_cents);
        setDisplayAmt(_cents);
      }
    } else if (isNaN(key)) {
      e.stopPropagation();
      return;
    } else {
      // Add key to the right and shift Amount left add 2 decimals
      let _cents = centAMT + key;
      setcentAMT(_cents);
      setDisplayAmt(_cents);
    }
  }

  function setDisplayAmt(cents: any) {
    const valueDisplay = (Number(cents) / 100).toLocaleString('en-US', {
      style: 'currency',
      currency: 'USD'
    });
    setAMT(valueDisplay);
  }

  return (
    <View>
      <View
        style={[
          styles.flex_row_space_between,
          { marginBottom: -16, marginTop: 2 }
        ]}
      >
        <View
          style={
            err_CC !== ''
              ? styles.textInputFieldErrorNW
              : styles.textInputFieldDarkNW
          }
        >
          <Image source={cardIcon} style={styles.cardIcon} />
          <TextInput
            placeholder="Card number"
            value={CC}
            style={[
              styles.textInputCard,
              {
                width: deviceWidth - 40,
                fontSize: fontSize
              }
            ]}
            onKeyPress={(e: any) => keyPressCardNumber(e)}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholderTextColor="rgba(0,0,0,0.35)"
            autoCapitalize={'none'}
            autoCorrect={false}
          />
        </View>
      </View>
      <View
        style={[
          styles.flex_row_space_between,
          { marginBottom: -16, marginTop: 2 }
        ]}
      >
        <View
          style={
            err_MMYY !== ''
              ? styles.textInputFieldErrorNW
              : styles.textInputFieldDarkNW
          }
        >
          <TextInput
            placeholder="MM / YY"
            value={MMYY}
            style={[styles.textInputCard, { width: 130, fontSize: fontSize }]}
            onKeyPress={(e: any) => keyPressExpMMYY(e)}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholderTextColor="rgba(0,0,0,0.35)"
            autoCapitalize={'none'}
            autoCorrect={false}
          />
        </View>
        <View
          style={
            err_CVV !== ''
              ? styles.textInputFieldErrorNW
              : styles.textInputFieldDarkNW
          }
        >
          <TextInput
            placeholder="CVV"
            value={CVV}
            style={[styles.textInputCard, { width: 90, fontSize: fontSize }]}
            onKeyPress={(e: any) => keyPressCVV(e)}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholderTextColor="rgba(0,0,0,0.35)"
            autoCapitalize={'none'}
            autoCorrect={false}
          />
        </View>
        <View
          style={
            err_ZIP !== ''
              ? styles.textInputFieldErrorNW
              : styles.textInputFieldDarkNW
          }
        >
          <TextInput
            placeholder="Zip"
            value={ZIP}
            style={[styles.textInputCard, { width: 120, fontSize: fontSize }]}
            onKeyPress={(e: any) => keyPressZIP(e)}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholderTextColor="rgba(0,0,0,0.35)"
            autoCapitalize={'none'}
            autoCorrect={false}
          />
        </View>
      </View>
      <View
        style={[
          styles.flex_row_space_between,
          { marginBottom: -16, marginTop: 2 }
        ]}
      >
        <View
          style={
            err_NAME !== ''
              ? styles.textInputFieldErrorNW
              : styles.textInputFieldDarkNW
          }
        >
          <TextInput
            placeholder="Name on Card"
            value={NAME}
            style={[
              styles.textInputCard,
              { width: deviceWidth - 40, fontSize: fontSize }
            ]}
            onKeyPress={() => setErrNAME('')}
            onChangeText={(v: string) => setNAME(v)}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholderTextColor="rgba(0,0,0,0.35)"
            autoCapitalize={'none'}
            autoCorrect={false}
          />
        </View>
      </View>
      <View
        style={[
          styles.flex_row_space_between,
          { marginBottom: -16, marginTop: 2 }
        ]}
      >
        <View
          style={
            err_PHONE !== ''
              ? styles.textInputFieldErrorNW
              : styles.textInputFieldDarkNW
          }
        >
          <TextInput
            placeholder="# Phone"
            value={PHONE}
            style={[
              styles.textInputCard,
              { width: deviceWidth - 40, fontSize: fontSize }
            ]}
            onKeyPress={() => setErrPHONE('')}
            onChangeText={(v: string) => setPHONE(v)}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholderTextColor="rgba(0,0,0,0.35)"
            autoCapitalize={'none'}
            autoCorrect={false}
          />
        </View>
      </View>
      <View
        style={[
          styles.flex_row_space_between,
          { marginBottom: -16, marginTop: 2 }
        ]}
      >
        <View
          style={
            err_EMAIL !== ''
              ? styles.textInputFieldErrorNW
              : styles.textInputFieldDarkNW
          }
        >
          <TextInput
            placeholder="@ Email"
            value={EMAIL}
            style={[
              styles.textInputCard,
              { width: deviceWidth - 40, fontSize: fontSize }
            ]}
            onKeyPress={() => setErrEMAIL('')}
            onChangeText={(v: string) => setEMAIL(v)}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholderTextColor="rgba(0,0,0,0.35)"
            autoCapitalize={'none'}
            autoCorrect={false}
          />
        </View>
      </View>
      <View
        style={[
          styles.flex_row_space_between,
          { marginBottom: -16, marginTop: 2 }
        ]}
      >
        <View
          style={
            err_AMT !== ''
              ? styles.textInputFieldErrorNW
              : styles.textInputFieldDarkNW
          }
        >
          <TextInput
            placeholder="$Amount"
            value={AMT}
            style={[
              styles.textInputCard,
              { width: deviceWidth * 0.6 - 20, fontSize: fontSize + 2 }
            ]}
            onKeyPress={(e: any) => keyPressAMT(e)}
            underlineColorAndroid="rgba(0,0,0,0)"
            placeholderTextColor="rgba(0,0,0,0.35)"
            autoCapitalize={'none'}
            autoCorrect={false}
          />
        </View>
        <Button
          text={'PAY'}
          width={deviceWidth * 0.4 - 30}
          height={44}
          fontSize={fontSize}
          backgroundColor={colors.green}
          busy={busy}
          onClick={submit}
        />
      </View>
      {result === 'SUCCESS' && (
        <View style={[styles.flex_row_center, { margin: 10 }]}>
          <Image
            source={require('../../assets/images/ok_icon.png')}
            style={[{ width: 40, height: 40 }]}
          />
          <Text style={styles.payment_success_text}>{result}</Text>
        </View>
      )}
      {result === 'ERROR' && (
        <View style={[styles.flex_row_center, { margin: 10 }]}>
          <Image
            source={require('../../assets/images/error_icon.png')}
            style={[{ width: 40, height: 40, opacity: 0.7 }]}
          />
          <Text style={styles.payment_error_text}>{result}</Text>
        </View>
      )}
    </View>
  );
};

export default CardInput;

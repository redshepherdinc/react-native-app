import React from 'react';
import { View } from 'react-native';

// styles
import styles from '../styles/common';
import { deviceWidth } from '../styles/variables';

// components
import CardInput from '../components/card_input';

const ScreenNewPayment = () => {
  return (
    <View style={[styles.page, { backgroundColor: '#11394f' }]}>
      <View style={[styles.flex_center, styles.mt60]}>
        <View
          style={[
            styles.shadowBox,
            {
              width: deviceWidth - 20,
              backgroundColor: '#f1f1f1',
              paddingTop: 10
            }
          ]}
        >
          <CardInput />
        </View>
      </View>
    </View>
  );
};

export default ScreenNewPayment;

import { Dimensions, Platform } from "react-native";

const dimensions = Dimensions.get("window");
const deviceHeight = dimensions.height;
const deviceWidth = dimensions.width;

const STATUSBAR_HEIGHT =
  Platform.OS === "ios" &&
  !Platform.isPad &&
  !Platform.isTVOS &&
  (dimensions.height === 812 || dimensions.width === 812)
    ? 22
    : 0;

const colors = {
  white: "#fff",
  offwhite: "#efefef",
  lightbluebg: "#EBF5FB",
  black: "rgb(19,19,19)",
  darkWhite: "rgba(255,255,255,0.6)",
  grey: "#404040",
  lightGrey: "rgb(150,150,150)",
  softBlue: "rgb(75,102,234)",
  darkSkyBlue: "rgb(63,103,230)",
  periBlue: "rgb(79,109,230)",
  red: "rgb(255,16,0)",
  borderColor: "rgb(229,229,229)",
  green: "#17B890", //'#16A085',//'#1bb690',
  errorBorder: "#F50046",
  darkGray: "#404040",

  //flat colors
  seagreen: "#14b7ac", //'#16A085',
  nephritis: "#27AE60",
  error: "#cc0000",
  lightred: "#ff6666",
  darkred: "#cc6969",
};

let lineHeight = {
  title: 38,
  header: 30,
  itemHeader: 29,
  normal: 23,
  small: 30,
};

const fontSize = {
  extraLarge: 32,
  title: 30,
  headerx: 22,
  header: 20,
  medium: 16,
  normal: 15,
  small: 13,
  xsmall: 12,
  xxsmall: 10,
};

const fontFamily = {
  light: "Poppins-Light",
  regular: "Poppins-Regular",
  medium: "Poppins-Medium",
  semiBold: "Poppins-SemiBold",
  extraBold: "Poppins-Bold",
};

export {
  deviceHeight,
  STATUSBAR_HEIGHT,
  deviceWidth,
  colors,
  fontSize,
  fontFamily,
  lineHeight,
};

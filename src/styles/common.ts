import { StyleSheet, Platform } from 'react-native';

import {
  deviceHeight,
  deviceWidth,
  colors,
  fontSize,
  fontFamily
} from './variables';

// CommonStyles
export default StyleSheet.create({
  // #region Hermes Engine
  hermes_engine: {
    position: 'absolute',
    right: 0
  },
  hermes_footer: {
    color: 'darkslategrey',
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right'
  },
  //#endregion

  //#region Page Styles
  page: {
    flex: 1
  },
  //#endregion

  //#region Flex styles
  flex1: {
    flex: 1
  },
  flex: {
    flex: 1
  },
  flex_center: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  flex_row_start: {
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  flex_row_center: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  flex_row_end: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  flex_row_space_between: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  flex_col_space_between: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  flex_col_center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  flex_col_center_top: {
    justifyContent: 'flex-start'
  },
  flex_col_left: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  //#endregion

  //#region Font Styles
  header_text_white: {
    color: colors.offwhite,
    fontSize: fontSize.header,
    fontFamily: fontFamily.semiBold,
    letterSpacing: 1,
    backgroundColor: 'transparent'
  },
  signup_text: {
    padding: 5,
    fontSize: fontSize.normal,
    letterSpacing: 1.5,
    backgroundColor: 'transparent',
    color: colors.green,
    fontFamily: fontFamily.light
  },
  payment_success_text: {
    padding: 5,
    fontSize: fontSize.headerx,
    letterSpacing: 1.5,
    backgroundColor: 'transparent',
    color: colors.nephritis,
    fontFamily: fontFamily.medium
  },
  payment_error_text: {
    padding: 5,
    fontSize: fontSize.headerx,
    letterSpacing: 1.5,
    backgroundColor: 'transparent',
    color: colors.error,
    fontFamily: fontFamily.medium,
    opacity: 0.75
  },
  //#endregion

  //#region Containers
  formbox: {
    padding: 10,
    alignItems: 'center'
  },
  chartbox: {
    borderRadius: 10
  },
  shadowBox: {
    marginHorizontal: 10,
    marginVertical: 5,
    marginBottom: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 10,
    backgroundColor: '#fff',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0,0.2)',
        shadowOffset: {
          width: 0,
          height: 5
        },
        shadowRadius: 5,
        shadowOpacity: 0.4
      },
      android: {
        elevation: 5
      }
    })
  },
  bluecontainer: {
    margin: 10,
    marginBottom: 0,
    padding: 10,
    backgroundColor: '#4E6D87',
    borderRadius: 10,
    width: deviceWidth - 55
  },
  bluecontainerslim: {
    backgroundColor: '#4E6D87',
    borderRadius: 4
  },
  //#endregion

  //#region Form Inputs
  cardIcon: {
    opacity: 1,
    position: 'absolute',
    bottom: 2,
    right: 2,
    width: 60,
    height: 38,
    borderRadius: 6
  },
  textInputIcon: {
    opacity: 0.75,
    position: 'absolute',
    bottom: 10,
    left: 10,
    width: 24,
    height: 24
  },
  textInputField: {
    flexDirection: 'row',
    width: deviceWidth - 55,
    height: 44,
    marginBottom: 22,
    borderColor: colors.borderColor,
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 6,
    backgroundColor: colors.white
  },
  textInputFieldDark: {
    flexDirection: 'row',
    width: deviceWidth - 55,
    height: 44,
    marginBottom: 22,
    borderColor: colors.borderColor,
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 6
  },
  textInputFieldDarkNH: {
    flexDirection: 'row',
    marginBottom: 22,
    borderColor: colors.borderColor,
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 6
  },
  textInputFieldDarkNW: {
    flexDirection: 'row',
    height: 45,
    marginBottom: 22,
    borderColor: colors.borderColor,
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 6
  },
  textInputFieldLight: {
    flexDirection: 'row',
    width: deviceWidth - 55,
    height: 44,
    marginBottom: 22,
    borderColor: 'rgb(191,191,191)',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 6
  },
  textInput: {
    width: deviceWidth - 55,
    height: 44,
    paddingLeft: 50,
    color: colors.darkGray,
    fontSize: fontSize.medium,
    fontFamily: fontFamily.regular,
    letterSpacing: 2
  },
  textInputDark: {
    width: deviceWidth - 55,
    height: 44,
    paddingLeft: 50,
    fontSize: fontSize.medium,
    fontFamily: fontFamily.regular,
    letterSpacing: 2,
    paddingHorizontal: 10,
    backgroundColor: 'rgba(255,255,255,0.1)',
    color: '#FFFFFF',
    borderStyle: 'solid',
    borderRadius: 6
  },
  textInputLight: {
    width: deviceWidth - 55,
    height: 44,
    paddingLeft: 50,
    fontSize: fontSize.medium,
    fontFamily: fontFamily.regular,
    letterSpacing: 2,
    paddingHorizontal: 10,
    backgroundColor: 'rgba(255,255,255,0.1)',
    color: '#000000',
    borderStyle: 'solid',
    borderRadius: 6
  },
  textInputFieldError: {
    flexDirection: 'row',
    width: deviceWidth - 55,
    height: 44,
    marginBottom: 22,
    borderColor: colors.error,
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 6
  },
  textInputFieldErrorNW: {
    flexDirection: 'row',
    height: 45,
    marginBottom: 22,
    borderColor: colors.error,
    backgroundColor: '#FFE4E1',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 6
  },
  textInputCard: {
    paddingLeft: 6,
    fontSize: 25,
    letterSpacing: 4,
    paddingHorizontal: 10,
    backgroundColor: 'rgba(0,0,0,0.08)',
    color: '#000000',
    borderStyle: 'solid',
    borderRadius: 6,
    height: 45
  },
  listInputFieldDark: {
    flexDirection: 'row',
    width: deviceWidth - 55,
    maxHeight: 190,
    marginBottom: 22,
    marginTop: -6,
    paddingTop: 8,
    borderColor: colors.borderColor,
    borderStyle: 'solid',
    backgroundColor: 'rgba(255,255,255,0.04)',
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20
  },
  listInputFieldLight: {
    flexDirection: 'row',
    width: deviceWidth - 55,
    maxHeight: 190,
    marginBottom: 22,
    marginTop: -6,
    paddingTop: 8,
    borderColor: 'rgb(191,191,191)',
    borderStyle: 'solid',
    backgroundColor: 'rgba(255,255,255,0.04)',
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20
  },

  listviewInputDark: {
    width: deviceWidth
  },
  listInputDark: {
    width: deviceWidth,
    height: 44,
    padding: 10,
    paddingLeft: 20,
    fontSize: fontSize.medium,
    fontFamily: fontFamily.regular,
    letterSpacing: 2,
    paddingHorizontal: 10,
    color: '#FFFFFF',
    borderStyle: 'solid'
  },
  listInputLight: {
    width: deviceWidth,
    height: 44,
    padding: 10,
    paddingLeft: 20,
    fontSize: 13,
    fontFamily: fontFamily.regular,
    letterSpacing: 2,
    paddingHorizontal: 10,
    color: '#000',
    borderStyle: 'solid'
  },
  //#endregion

  //#region Buttons
  buttonBox: {
    height: 45,
    alignItems: 'center'
  },
  buttonBoxSm: {
    height: 25,
    alignItems: 'center'
  },
  buttontext: {
    fontFamily: fontFamily.regular,
    margin: 1,
    letterSpacing: 1
  },
  button: {
    paddingTop: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  //#endregion

  //#region Shadows
  shadow: {
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0,0.25)',
        shadowOffset: {
          width: 0,
          height: 5
        },
        shadowRadius: 5,
        shadowOpacity: 0.4
      },
      android: {
        elevation: 5
      }
    })
  },
  shadowDark: {
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0,0.5)',
        shadowOffset: {
          width: 0,
          height: 5
        },
        shadowRadius: 5,
        shadowOpacity: 0.8
      },
      android: {
        elevation: 5,
        backgroundColor: 'transparent'
      }
    })
  },
  //#endregion

  //#region Width
  w10: {
    width: 10
  },
  w20: {
    width: 20
  },
  w100: {
    width: 100
  },
  w150: {
    width: 150
  },
  w200: {
    width: 200
  },
  w250: {
    width: 250
  },
  //#endregion

  //#region Padding
  p1: {
    padding: 1
  },
  p2: {
    padding: 2
  },
  p3: {
    padding: 3
  },
  p4: {
    padding: 4
  },
  p5: {
    padding: 5
  },
  p10: {
    padding: 10
  },
  p15: {
    padding: 15
  },
  p20: {
    padding: 20
  },

  pt1: {
    paddingTop: 1
  },
  pt2: {
    paddingTop: 2
  },
  pt3: {
    paddingTop: 3
  },
  pt4: {
    paddingTop: 4
  },
  pt5: {
    paddingTop: 5
  },
  pt10: {
    paddingTop: 10
  },
  pt15: {
    paddingTop: 15
  },
  pt20: {
    paddingTop: 20
  },

  pb1: {
    paddingBottom: 1
  },
  pb2: {
    paddingBottom: 2
  },
  pb3: {
    paddingBottom: 3
  },
  pb4: {
    paddingBottom: 4
  },
  pb5: {
    paddingBottom: 5
  },
  pb10: {
    paddingBottom: 10
  },
  pb15: {
    paddingBottom: 15
  },
  pb20: {
    paddingBottom: 20
  },

  pl1: {
    paddingLeft: 1
  },
  pl2: {
    paddingLeft: 2
  },
  pl3: {
    paddingLeft: 3
  },
  pl4: {
    paddingLeft: 4
  },
  pl5: {
    paddingLeft: 5
  },
  pl10: {
    paddingLeft: 10
  },
  pl15: {
    paddingLeft: 15
  },
  pl20: {
    paddingLeft: 20
  },

  pr1: {
    paddingRight: 1
  },
  pr2: {
    paddingRight: 2
  },
  pr3: {
    paddingRight: 3
  },
  pr4: {
    paddingRight: 4
  },
  pr5: {
    paddingRight: 5
  },
  pr10: {
    paddingRight: 10
  },
  pr15: {
    paddingRight: 15
  },
  pr20: {
    paddingRight: 20
  },

  ph1: {
    paddingHorizontal: 1
  },
  ph2: {
    paddingHorizontal: 2
  },
  ph3: {
    paddingHorizontal: 3
  },
  ph4: {
    paddingHorizontal: 4
  },
  ph5: {
    paddingHorizontal: 5
  },
  ph10: {
    paddingHorizontal: 10
  },
  ph15: {
    paddingHorizontal: 15
  },
  ph20: {
    paddingHorizontal: 20
  },

  pv1: {
    paddingVertical: 1
  },
  pv2: {
    paddingVertical: 2
  },
  pv3: {
    paddingVertical: 3
  },
  pv4: {
    paddingVertical: 4
  },
  pv5: {
    paddingVertical: 5
  },
  pv10: {
    paddingVertical: 10
  },
  pv15: {
    paddingVertical: 15
  },
  pv20: {
    paddingVertical: 20
  },
  //#endregion

  //#region Margin
  m1: {
    margin: 1
  },
  m2: {
    margin: 2
  },
  m3: {
    margin: 3
  },
  m4: {
    margin: 4
  },
  m5: {
    margin: 5
  },
  m10: {
    margin: 10
  },
  n_m10: {
    margin: -10
  },
  m15: {
    margin: 15
  },
  m20: {
    margin: 20
  },

  mt1: {
    marginTop: 1
  },
  mt2: {
    marginTop: 2
  },
  mt3: {
    marginTop: 3
  },
  mt4: {
    marginTop: 4
  },
  mt5: {
    marginTop: 5
  },
  mt6: {
    marginTop: 6
  },
  mt7: {
    marginTop: 7
  },
  mt8: {
    marginTop: 8
  },
  mt10: {
    marginTop: 10
  },
  mtn10: {
    marginTop: -10
  },
  mt15: {
    marginTop: 15
  },
  mt20: {
    marginTop: 20
  },
  mt30: {
    marginTop: 30
  },
  mt40: {
    marginTop: 40
  },
  mt50: {
    marginTop: 50
  },
  mt60: {
    marginTop: 60
  },
  mtn60: {
    marginTop: -60
  },
  mt100: {
    marginTop: 100
  },
  mtn100: {
    marginTop: -100
  },
  mb0: {
    marginBottom: 0
  },
  mb1: {
    marginBottom: 1
  },
  mb2: {
    marginBottom: 2
  },
  mb3: {
    marginBottom: 3
  },
  mb4: {
    marginBottom: 4
  },
  mb5: {
    marginBottom: 5
  },
  mb10: {
    marginBottom: 10
  },
  mb15: {
    marginBottom: 15
  },
  mb20: {
    marginBottom: 20
  },

  ml1: {
    marginLeft: 1
  },
  ml2: {
    marginLeft: 2
  },
  ml3: {
    marginLeft: 3
  },
  ml4: {
    marginLeft: 4
  },
  ml5: {
    marginLeft: 5
  },
  ml10: {
    marginLeft: 10
  },
  ml15: {
    marginLeft: 15
  },
  ml20: {
    marginLeft: 20
  },

  mr1: {
    marginRight: 1
  },
  mr2: {
    marginRight: 2
  },
  mr3: {
    marginRight: 3
  },
  mr4: {
    marginRight: 4
  },
  mr5: {
    marginRight: 5
  },
  mr10: {
    marginRight: 10
  },
  mr15: {
    marginRight: 15
  },
  mr20: {
    marginRight: 20
  },

  mh1: {
    marginHorizontal: 1
  },
  mh2: {
    marginHorizontal: 2
  },
  mh3: {
    marginHorizontal: 3
  },
  mh4: {
    marginHorizontal: 4
  },
  mh5: {
    marginHorizontal: 5
  },
  mh10: {
    marginHorizontal: 10
  },
  mh15: {
    marginHorizontal: 15
  },
  mh20: {
    marginHorizontal: 20
  },

  mv1: {
    marginVertical: 1
  },
  mv2: {
    marginVertical: 2
  },
  mv3: {
    marginVertical: 3
  },
  mv4: {
    marginVertical: 4
  },
  mv5: {
    marginVertical: 5
  },
  mv10: {
    marginVertical: 10
  },
  n_mv10: {
    marginVertical: -10
  },
  mv15: {
    marginVertical: 15
  },
  mv20: {
    marginVertical: 20
  }
  //#endregion
});

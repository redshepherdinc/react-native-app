import React, { useState, useEffect } from "react";
import { TouchableOpacity, Text, View, ActivityIndicator } from "react-native";

import styles from "../styles/common";
import { fontFamily } from "../styles/variables";

type Props = {
  text?: string;
  backgroundColor?: string;
  textColor?: string;
  fontSize?: number;
  width?: number;
  height?: number;
  borderRadius?: number;
  busy?: boolean;
  onClick?: () => void;
  style?: any;
};

const Button = (props: Props) => {
  const buttonStyle = {
    backgroundColor: props.backgroundColor ?? "#38c4a8",
    width: props.width ?? 100,
    height: props.height ?? 30,
    borderRadius: 6, //(props.height ?? 30) / 2,
  };

  const textStyle = {
    color: props.textColor ?? "white",
    fontSize: props.fontSize ?? 14,
    fontFamily: fontFamily.regular,
    letterSpacing: 2,
  };

  function onPress() {
    if (props.onClick !== null && props.onClick !== undefined) props.onClick();
  }

  return (
    <TouchableOpacity
      style={props.style}
      activeOpacity={0.8}
      onPress={() => onPress()}
    >
      <View style={[styles.shadowDark, styles.button, buttonStyle]}>
        {props.busy ? (
          <ActivityIndicator color="#fffff" />
        ) : (
          <Text style={[styles.buttontext, styles.shadow, textStyle]}>
            {props.text ?? "BUTTON"}
          </Text>
        )}
      </View>
    </TouchableOpacity>
  );
};

export default Button;

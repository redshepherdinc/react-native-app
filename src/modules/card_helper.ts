export function getCardName(t: string) {
  let r = '';
  switch (t) {
    case 'V':
      r = 'Visa';
      break;
    case 'M':
      r = 'Master';
      break;
    case 'N':
      r = 'Diners';
      break;
    case 'B':
      r = 'CarteBlanche';
      break;
    case 'A':
      r = 'AMEX';
      break;
    case 'D':
      r = 'Discover';
      break;
    case 'J':
      r = 'JCB';
      break;
    case 'R':
      r = 'Enroute';
      break;
    case 'S':
      r = 'Solo';
      break;
    case 'W':
      r = 'Switch';
      break;
    case 'T':
      r = 'Maestro';
      break;
    case 'E':
      r = 'Visa E';
      break;
    case 'L':
      r = 'Laser';
      break;
  }
  return r;
}

export function addSpacers(cardnumber: string, cardType: any, spacer = ' ') {
  cardnumber = cardnumber.replace(/ /g, '');
  let len = cardnumber.length;
  let res = '';

  if (cardType && cardType.name === 'AmEx') {
    if (len < 4) res += cardnumber;
    else if (len >= 4 && len < 10)
      res += cardnumber.substring(0, 4) + spacer + cardnumber.substring(4);
    else if (len >= 10)
      res +=
        cardnumber.substring(0, 4) +
        spacer +
        cardnumber.substring(4, 10) +
        spacer +
        cardnumber.substring(10);
  } else {
    if (len < 4) res += cardnumber;
    else if (len >= 4 && len < 8)
      res += cardnumber.substring(0, 4) + spacer + cardnumber.substring(4);
    else if (len >= 8 && len < 12)
      res +=
        cardnumber.substring(0, 4) +
        spacer +
        cardnumber.substring(4, 8) +
        spacer +
        cardnumber.substring(8);
    if (len >= 12)
      res +=
        cardnumber.substring(0, 4) +
        spacer +
        cardnumber.substring(4, 8) +
        spacer +
        cardnumber.substring(8, 12) +
        spacer +
        cardnumber.substring(12);
  }

  return res;
}

export function getCardType(cardnumber: string) {
  // Array to hold the permitted card characteristics
  var cards = new Array();
  // Define the cards we support. You may add addtional card types as follows.
  //  Name:         As in the selection box of the form - must be same as user's
  //  Length:       List of possible valid lengths of the card number for the card
  //  prefixes:     List of possible prefixes for the card
  //  checkdigit:   Boolean to say whether there is a check digit
  cards[0] = {
    name: 'Visa',
    lengths: '13,16',
    prefixes: '4',
    checkdigit: true
  };
  cards[1] = {
    name: 'MasterCard',
    lengths: '16',
    prefixes: '51,52,53,54,55',
    checkdigit: true
  };
  cards[2] = {
    name: 'DinersClub',
    lengths: '14,16',
    prefixes: '36,38,54,55',
    checkdigit: true
  };
  cards[3] = {
    name: 'CarteBlanche',
    lengths: '14',
    prefixes: '300,301,302,303,304,305',
    checkdigit: true
  };
  cards[4] = {
    name: 'AmEx',
    lengths: '15',
    prefixes: '34,37',
    checkdigit: true
  };
  cards[5] = {
    name: 'Discover',
    lengths: '16',
    prefixes: '6011,622,64,65',
    checkdigit: true
  };
  cards[6] = {
    name: 'JCB',
    lengths: '16',
    prefixes: '35',
    checkdigit: true
  };
  cards[7] = {
    name: 'enRoute',
    lengths: '15',
    prefixes: '2014,2149',
    checkdigit: true
  };
  cards[8] = {
    name: 'Solo',
    lengths: '16,18,19',
    prefixes: '6334,6767',
    checkdigit: true
  };
  cards[9] = {
    name: 'Switch',
    lengths: '16,18,19',
    prefixes: '4903,4905,4911,4936,564182,633110,6333,6759',
    checkdigit: true
  };
  cards[10] = {
    name: 'Maestro',
    lengths: '12,13,14,15,16,18,19',
    prefixes: '5018,5020,5038,6304,6759,6761,6762,6763',
    checkdigit: true
  };
  cards[11] = {
    name: 'VisaElectron',
    lengths: '16',
    prefixes: '4026,417500,4508,4844,4913,4917',
    checkdigit: true
  };
  cards[12] = {
    name: 'LaserCard',
    lengths: '16,17,18,19',
    prefixes: '6304,6706,6771,6709',
    checkdigit: true
  };

  var cnum = cardnumber;
  cnum = cnum.replace(/\s/g, '');

  var cardType = -1;
  for (var i = 0; i < cards.length; i++) {
    var prefixes = cards[i].prefixes.split(',');

    for (var j = 0; j < prefixes.length; j++) {
      if (cnum.startsWith(prefixes[j])) {
        cardType = i;
        break;
      }

      if (cardType != -1) {
        break;
      }
    }
  }

  if (cardType != -1) {
    return cards[cardType];
  }

  return null;
}

export function isCardValid(cardnumber: string) {
  var cnum = cardnumber;
  cnum = cnum.replace(/ /g, '');
  var countnum = cnum.replace(/[^0-9]/g, '').length;

  var ccErrorNo = 0;
  var ccErrors = new Array();

  ccErrors[0] = 'Unknown card type';
  ccErrors[1] = 'No card number provided';
  ccErrors[2] = 'Credit card number format invalid';
  ccErrors[3] = 'Credit card number is invalid';
  ccErrors[4] = 'Credit card invalid number of digits';
  ccErrors[5] = 'Warning! scam attempt';

  // Ensure that the user has provided a credit card number
  if (countnum == 0) {
    return ccErrors[1];
  }

  // If card type not found, report an error
  var cardType = getCardType(cnum);
  if (cardType === null) {
    return ccErrors[0];
  }

  // Check that the number is numeric
  var cardexp = /^[0-9]{13,19}$/;
  if (!cardexp.exec(cnum)) {
    return ccErrors[2];
  }

  // Now check the modulus 10 check digit - if required
  if (cardType.checkdigit) {
    var checksum = 0; // running checksum total
    var mychar = ''; // next char to process
    var j = 1; // takes value of 1 or 2

    // Process each digit one by one starting at the right
    var calc;
    for (var i = cnum.length - 1; i >= 0; i--) {
      // Extract the next digit and multiply by 1 or 2 on alternative digits.
      calc = Number(cnum.charAt(i)) * j;

      // If the result is in two digits add 1 to the checksum total
      if (calc > 9) {
        checksum = checksum + 1;
        calc = calc - 10;
      }

      // Add the units element to the checksum total
      checksum = checksum + calc;

      // Switch the value of j
      if (j == 1) {
        j = 2;
      } else {
        j = 1;
      }
    }

    // All done - if checksum is divisible by 10, it is a valid modulus 10. If not, report an error.
    if (checksum % 10 != 0) {
      return ccErrors[3];
    }
  }

  // Check it's not a spam number
  if (cnum == '5490997771092064') {
    return ccErrors[5];
  }

  // The following are the card-specific checks we undertake.
  var lengthValid = false;
  var prefixValid = false;

  // We use these for holding the valid lengths and prefixes of a card type
  var prefix = new Array();
  var lengths = new Array();

  // Load an array with the valid prefixes for this card
  prefix = cardType.prefixes.split(',');

  // Now see if any of them match what we have in the card number
  for (i = 0; i < prefix.length; i++) {
    var exp = new RegExp('^' + prefix[i]);
    if (exp.test(cnum)) prefixValid = true;
  }

  // If it isn't a valid prefix there's no point at looking at the length
  if (!prefixValid) {
    return ccErrors[3];
  }

  // See if the length is valid for this card
  lengths = cardType.lengths.split(',');
  for (j = 0; j < lengths.length; j++) {
    if (cnum.length == lengths[j]) lengthValid = true;
  }

  // See if all is OK by seeing if the length was valid. We only check the length if all else was
  // hunky dory.
  if (!lengthValid) {
    return ccErrors[4];
  }

  // The credit card is in the required format.
  return '';
}

export function isExpirationValid(mm: number, yyyy: number) {
  var today = new Date();
  var someday = new Date();

  someday.setFullYear(yyyy, mm, 1);

  if (someday < today) return false;

  return true;
}

export function formatCardNumber(cardnumber: string, spacer: string = ' ') {
  var cnum = cardnumber;
  cnum = cnum.replace(/ /g, '');
  var countnum = cnum.replace(/[^0-9]/g, '').length;
  var cardType = getCardType(cnum);

  if (cardType === null) {
    if (countnum === 4 || countnum === 8 || countnum === 12) {
      cardnumber += spacer;
    }
  } else {
    switch (cardType.name) {
      case 'AmEx':
        if (countnum === 4 || countnum === 10) {
          cardnumber += spacer;
        }
        break;

      default:
        if (countnum === 4 || countnum === 8 || countnum === 12) {
          cardnumber += spacer;
        }
        break;
    }
  }

  return cardnumber;
}

export function getCardNumber(cardnumber: string) {
  return cardnumber.replace(/\s/g, '');
}

export function getExpMMYYYY(mmyy: string) {
  let a = mmyy.split('/');

  if (a.length === 2) {
    return a[0].trim() + '20' + a[1].trim();
  } else return '';
}

export function getAmountInCents(amt: string) {
  let a = amt.replace('$', '');
  return (Number(a) * 100).toFixed(0);
}

export function isValidEmail(email: string) {
  var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

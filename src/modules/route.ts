export default class Route {
  rootUrl: string;

  constructor(rootUrl: string) {
    this.rootUrl = rootUrl;
  }

  get = (url: string) => {
    return fetch(this.rootUrl + url)
      .then(this.checkStatus)
      .then((response) => response.json())
      .catch((e) => e);
  };

  post = (url: string) => {
    return fetch(this.rootUrl + url, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      mode: "cors", // no-cors, *cors, same-origin
      credentials: "same-origin", // include, *same-origin, omit
    })
      .then(this.checkStatus)
      .then((response) => response.json())
      .catch((e) => e);
  };

  postdata = (url: string, data: any) => {
    // console.log("url >>>", this.rootUrl + url);
    // console.log("data >>>", JSON.stringify(data, null, 2));
    return fetch(this.rootUrl + url, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: "POST",
      mode: "cors", // no-cors, *cors, same-origin
      credentials: "same-origin", // include, *same-origin, omit
      body: JSON.stringify(data),
    })
      .then(this.checkStatus)
      .then((response) => response.json())
      .catch((e) => e);
  };

  postfile = (url: string, data: any) => {
    return fetch(this.rootUrl + url, {
      headers: {
        Accept: "application/json",
        "Content-Type": "multipart/form-data",
      },
      method: "POST",
      mode: "cors", // no-cors, *cors, same-origin
      credentials: "same-origin", // include, *same-origin, omit
      body: data,
    })
      .then(this.checkStatus)
      .then((response) => response.json())
      .catch((e) => e);
  };

  postfile_x = (url: string, data: any) => {
    return fetch(this.rootUrl + url, {
      headers: {
        Accept: "application/json",
        "Content-Type": "multipart/form-data",
      },
      method: "POST",
      mode: "cors", // no-cors, *cors, same-origin
      credentials: "same-origin", // include, *same-origin, omit
      body: data,
    });
  };

  checkStatus = (response: any) => {
    //console.log('checkStatus response >>>', response);
    if (response.ok) {
      return response;
    } else if (response.status === 400) {
      let error = new Error("Error: Not found");

      try {
        if (
          response.headers &&
          response.headers.map &&
          response.headers.map.err
        ) {
          //console.log('header err >>>', response.headers.map.err);
          let _err = String(response.headers.map.err);
          response._bodyText = _err;
          let error = new Error(response._bodyText);
          error = response;
          throw error;
        }
      } catch (e) {
        /* no op */
      }

      throw error;
    } else {
      let error = new Error(response.statusText);
      error.stack = response;
      throw error;
    }
  };
}

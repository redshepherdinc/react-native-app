/*! RedPay API v2.1.0 | (c) 2017 Red Shepherd Inc. | www.redshepherd.com/license | www.redshepherd.com/docs/js | support@redshepherd.com */
/*! STANDALONE RedPay Api library */
/*! RedPay API v2.5.2 | (c) 2018 Red Shepherd Inc. | www.redshepherd.com/license | support@redshepherd.com */

import CryptoJS from 'crypto-js';

// route
import Route from './route';

export interface Config {
  app: string;
  url: string;
  key: string;
}

export module RedPayProcessor {
  export function Charge(config: Config, redPayRequest: any) {
    /* CHARGE Structure
    {
      amount, // default: 0
      account,
      currency, // default: "USD"
      expmmyyyy,
      cvv,
      signatureData,
      cardHolderName,
      method, // default: "CNP"
      authCode,
      retryCount,
      track1Data,
      track2Data,
      avsAddress1,
      avsAddress2,
      avsCity,
      avsZip,
      cardHolderEmail,
      cardHolderPhone,
      ref1,
      ref2,
      ref3,
      ref4,
      ref5
    }
    */

    redPayRequest.action = 'A';
    redPayRequest.amount = redPayRequest.amount || 0;
    redPayRequest.currency = redPayRequest.currency || 'USD';
    redPayRequest.method = redPayRequest.method || 'CNP';
    redPayRequest.productRef = redPayRequest.productRef || 'E';

    return SendRequest(config, redPayRequest, '/ecard');
  }

  function SendRequest(config: Config, redPayRequest: any, path: string) {
    var redpay_route = new Route(config.url);

    var randomSecretPassphraseBase64 = Crypto.getBase64String(
      Crypto.generateRandomAesKeyBytes()
    );

    return new Promise(function (resolve, reject) {
      // Step #1 Request RSA encrypted text for the random secret passphrase
      // This is because React Native does not have a proper RSA library,
      // we have a server side route to encrypt a secret passphrase key using our RSA Public key
      var ekeyReq = {
        rkey: config.key,
        data: randomSecretPassphraseBase64
      };
      redpay_route
        .postdata('ekey', ekeyReq)
        .then(({ ekey }: any) => {
          resolve(ekey);
        })
        .catch((err) => reject(err));
    })
      .then((result: any) => {
        // Step #2 Request Session for payment request
        return new Promise(function (resolve, reject) {
          var sessionReq = {
            rsaPublicKey: config.key,
            aesKey: result
          };
          //console.log("sessionReq >>>", JSON.stringify(sessionReq, null, 2));
          redpay_route
            .postdata('ecard', sessionReq)
            .then(({ sessionId }: any) => {
              resolve(sessionId);
            })
            .catch((err) => reject(err));
        });
      })
      .then((result: any) => {
        //console.log("sessionId >>>", result);
        //Step #3 Send Redpay payment request
        return new Promise(function (resolve, reject) {
          var randomIVBase64String = Crypto.getBase64String(
            Crypto.generateRandomIvBytes()
          );

          var aesPlainText = JSON.stringify(redPayRequest);

          var aesCipherText = Crypto.encrypt(
            Crypto.getBytes(randomSecretPassphraseBase64),
            Crypto.getBytes(randomIVBase64String),
            aesPlainText
          );

          var packet = {
            sessionId: result,
            app: config.app,
            aesData: aesCipherText,
            iv: randomIVBase64String
          };
          //console.log("packet >>>", JSON.stringify(packet, null, 2));

          redpay_route
            .postdata('ecard', packet)
            .then((encryptedResponse: any) => {
              //console.log("encryptedResponse >>>", encryptedResponse);
              var redPayResponse = Crypto.decrypt(
                Crypto.getBytes(randomSecretPassphraseBase64),
                Crypto.getBytes(encryptedResponse.iv),
                encryptedResponse.aesData
              );

              var jsonResponse = JSON.parse(redPayResponse);
              resolve(jsonResponse);
            })
            .catch((err) => reject(err));
        });
      });
  }

  var Crypto = {
    getBytes: function (base64String: any) {
      return CryptoJS.enc.Base64.parse(base64String);
    },
    getBase64String: function (byteArray: any) {
      return CryptoJS.enc.Base64.stringify(byteArray);
    },
    generateRandomIvBytes: function () {
      return CryptoJS.lib.WordArray.random(16);
    },
    generateRandomAesKeyBytes: function () {
      return CryptoJS.lib.WordArray.random(32);
    },
    encrypt: function (aesKeyBytes: any, ivBytes: any, plainText: any) {
      var encryptedBytes = CryptoJS.AES.encrypt(plainText, aesKeyBytes, {
        iv: ivBytes,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      });
      return encryptedBytes.toString();
    },
    decrypt: function (aesKeyBytes: any, ivBytes: any, cipherTextBase64: any) {
      var decrypt = CryptoJS.AES.decrypt(cipherTextBase64, aesKeyBytes, {
        iv: ivBytes,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      });
      var plaintext = decrypt.toString(CryptoJS.enc.Utf8);
      return plaintext;
    }
  };
}
